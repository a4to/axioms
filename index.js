/**
 *
 * @license Axioms.js
 * MIT/X Consortium License
 *
 * File: index.js
 * © Concise Computing <connor@concise.cc>
 *
 *
 * Usage:
 *
 * Import all functions and classes:
 * require('axioms.js')
 *
 * Import only functions:
 * require('axioms.js').functions()
 *
 * Import only classes:
 * require('axioms.js').classes()
 *
*/

// ----------------------------

const fs = require('fs');
const path = require('path');
const fsp = require('fs').promises;
const { promisify } = require('util');
const { exec, execSync, spawn } = require('child_process');

/* ----------------------------
 *          Functions
 * ----------------------------
*/

const log = input => {
  /* Colored console logs
   * Usage: log(':r:Red text:r: :g:Green text:g: :b:Blue text:b: :y:Yellow text:y: :x:Custom color text (terquoise):x: :c:Reset color:c:');
   */
  const colors = {
    r: '\x1b[31;1m', g: '\x1b[32;1m', b: '\x1b[34;1m', y: '\x1b[33;1m',
    m: '\x1b[35;1m', x: '\x1b[38;2;103;208;168m', c: '\x1b[0m'
  };
  let result = input.replace(/:([rgbyxmc]):/g, (match, p1) => colors[p1] || '');
  console.log(result + colors.c);
};

const write = (file, data) => fsp.writeFile
const read = file => fsp.readFile(file, 'utf8');
const copy = (src, dest) => fsp.copyFile(src, dest);
const remove = file => fsp.unlink(file);
const exists = file => fs.existsSync(file);
const rename = (oldPath, newPath) => fsp.rename(oldPath, newPath);
const move = (src, dest) => fsp.rename(src, dest);
const list = dir => fsp.readdir(dir);
const stat = file => fsp.stat(file);
const isDir = file => stat(file).then(stats => stats.isDirectory());
const isFile = file => stat(file).then(stats => stats.isFile());
const isSymLink = file => stat(file).then(stats => stats.isSymbolicLink());

const join = (...args) => path.join(...args);
const resolve = (...args) => path.resolve(...args);
const basename = file => path.basename(file);
const dirname = file => path.dirname(file);
const extname = file => path.extname(file);
const relative = (from, to) => path.relative(from, to);
const normalize = file => path.normalize(file);
const isAbsolute = file => path.isAbsolute(file);

const parse = json => JSON.parse(json);
const parseFile = file => JSON.parse(fs.readFileSync(file, 'utf8'));
const stringify = json => JSON.stringify(json, null, 2);
const writeJson = (file, data) => fs.writeFileSync(file, stringify(data), 'utf8');
const readJson = file => parse(fs.readFileSync(file, 'utf8'));

const mkdir = dir => !fs.existsSync(dir) && fs.mkdirSync(dir, { recursive: true });
const rmdir = dir => fs.existsSync(dir) && fs.rmdirSync(dir, { recursive: true });

const Functions = { log, write, read, copy, remove, exists, rename, move, list, stat, isDir, isFile, isSymLink, join, resolve, basename, dirname, extname, relative, normalize, isAbsolute, parse, parseFile, stringify, writeJson, readJson, mkdir, rmdir };

/* ----------------------------
 *          Classes
 * ----------------------------
*/

class Base {
  static colorize(obj) {
    const colors = { green: '\x1b[32m%s\x1b[0m', blue: '\x1b[38;2;103;208;168m%s\x1b[0m', magenta: '\x1b[35m%s\x1b[0m', cyan: '\x1b[36m%s\x1b[0m', yellow: '\x1b[33m%s\x1b[0m', gray: '\x1b[90m%s\x1b[0m' };
    const colorizeString = (str, color) => `${colors[color].replace('%s', str)}`;
    const colorizeHelper = (obj, indentLevel = 0, isFirstKey = true) => {
      const indent = ' '.repeat(indentLevel * 2);
      if (typeof obj === 'string') return colorizeString(`"${obj}"`, 'blue');
      else if (typeof obj === 'number') return colorizeString(obj, 'magenta');
      else if (typeof obj === 'boolean') return colorizeString(obj, 'cyan');
      else if (Array.isArray(obj)) return `[\n${indent}  ${obj.map(item => colorizeHelper(item, indentLevel + 1)).join(`,\n${indent}  `)}\n${indent}]`.replace(/\[\n\s*\]/g, '[]');
      else if (typeof obj === 'object' && obj !== null) {
        let result = `{\n`;
        Object.entries(obj).forEach(([key, value], index) => {
          const keyColor = key === 'constructor' ? 'cyan' : (isFirstKey && indentLevel === 0 ? 'green' : 'yellow'), coloredKey = colorizeString(`"${key}"`, keyColor); isFirstKey = false;
          result += `${indent}  ${coloredKey}: ${colorizeHelper(value, indentLevel + 1, false)}`;
          if (index < Object.entries(obj).length - 1) result += `,\n`;
        });
        result += `\n${indent}}`;
        return result;
      } else return colorizeString(obj.toString(), 'gray');
    };
    return colorizeHelper(obj);
  }

  static usage(print = true) {
    /* Usage method:
     * Returns the usage of the class as an object
     * Usage: MyClass.usage() or MyClass.usage(false)
    */
    const className = this.name;
    const staticMethods = Object.getOwnPropertyNames(this)
      .filter(prop => typeof this[prop] === 'function' && prop !== 'usage' && prop !== 'length' && prop !== 'name' && prop !== 'prototype')
      .reduce((acc, methodName) => {
        const fn = this[methodName];
        acc[methodName] = fn.toString().slice(fn.toString().indexOf('(') + 1, fn.toString().indexOf(')')).split(',').map(item => item.trim()).filter(item => item);
        return acc;
      }, {});

    const instanceMethods = Object.getOwnPropertyNames(this.prototype)
      .filter(prop => prop !== 'constructor' && typeof this.prototype[prop] === 'function')
      .reduce((acc, methodName) => {
        const fn = this.prototype[methodName];
        acc[methodName] = fn.toString().slice(fn.toString().indexOf('(') + 1, fn.toString().indexOf(')')).split(',').map(item => item.trim()).filter(item => item);
        return acc;
      }, {});

    const constructorParams = this.prototype.constructor.toString().slice(this.prototype.constructor.toString().indexOf('(') + 1, this.prototype.constructor.toString().indexOf(')')).split(',').map(item => item.trim()).filter(item => item);
    const result = {
      [className]: {
        "constructor": constructorParams,
        ...staticMethods,
        ...instanceMethods
      }
    };
    if (!print) return result;
    console.log(this.colorize(result));
    return result;
  }
}

const Classes = { Base };

/* ----------------------------
 *         Globalize
 * ----------------------------
*/

const functions = () => {
  for (let key in Functions) {
    Object.assign(global, { [key]: Functions[key] });
  }
}

const classes = () => {
  for (let key in Classes) {
    Object.assign(global, { [key]: Classes[key] });
  }
}

const index = { functions, classes }

const axioms = (type) => {
  if (type) index[type] && index[type]();
  else { functions(); classes() }
}

// ---------------------------------------------------------------------------------------------------- //

module.exports = axioms;

